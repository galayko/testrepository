﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.IO;
using System.Threading;

namespace NixFinalTask
{
    public delegate void SaveToXMLDelegate(string savePath);
    public delegate void GenerateTreeDelegate (TreeView treeView);
    public delegate ISynchronizeInvoke PrintException (UnauthorizedAccessException ex);
    public partial class Form1 : Form
    {
        string pathString, fileSaveString;
        event PrintException printEx;
        ManualResetEvent ScanningDone = new ManualResetEvent(false);
        DirectoryElem rootDirectory = new DirectoryElem();
        Thread CollectDirInfoThread, DirInfoToXMLThread, DirInfoToTreeView;

        public Form1()
        {
            InitializeComponent();
            printEx += this.PrintException;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirInfoToXMLThread = new Thread(new ThreadStart(DirInfoToXML));
            DirInfoToXMLThread.Name = "DirInfoToXMLThread";
            DirInfoToXMLThread.Start();
            DirInfoToTreeView = new Thread(new ThreadStart(DirInfoToTree));
            DirInfoToTreeView.Name = "DirInfoToTreeView";
            DirInfoToTreeView.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DirInfoToXMLThread.Abort();
            DirInfoToTreeView.Abort();
        }

        private void buttonChooseDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                pathString = fbd.SelectedPath;
                CollectDirInfoThread = new Thread(()=> GetDirInfo(pathString));
                CollectDirInfoThread.Name = "CollectDirInfoThread";
                CollectDirInfoThread.Start();
            }
        }

        private void buttonSaveToXml_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "XML|*.xml";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileSaveString = saveDialog.FileName.ToString();
                    buttonChooseDir.Enabled = true;
                }
                catch (Exception)
                {
                    MessageBox.Show("Файл выбран неправильно.");
                }
            }
        }

        private void GetDirInfo(string pathString)
        {
            lock (rootDirectory)
            {
                DirectoryInfo rootInfo = new DirectoryInfo(pathString);
                rootDirectory = GetDirectoryElem(rootInfo);
            }
            ScanningDone.Set();
        }

        private DirectoryElem GetDirectoryElem(DirectoryInfo directoryInfo)
        {
            try
            {
            DirectoryElem dir = new DirectoryElem(directoryInfo);
            foreach (var file in directoryInfo.GetFiles())
            {
                dir.subItems.Add(new FileElem(file));
            }
            foreach (var directory in directoryInfo.GetDirectories())
            {
                dir.subItems.Add(GetDirectoryElem(directory));
            }
            foreach (var element in dir.subItems) // Просчитаем размер для папки
            {
                dir.Length += element.Length;
            }
            return dir;
            }
            catch (UnauthorizedAccessException ex)
            {
                printEx(ex);
                return new DirectoryElem(directoryInfo);
            }
        }

        private void DirInfoToXML()
        {
            while (true)
            {
                ScanningDone.WaitOne();
                SaveToXMLDelegate asyncDel = SaveToXML;
                IAsyncResult ar = asyncDel.BeginInvoke(fileSaveString, null, null);
                asyncDel.EndInvoke(ar);
                ScanningDone.Reset();
                //MessageBox.Show("Saving done " + Thread.CurrentThread.ManagedThreadId);
            }
        }

        private void SaveToXML(string savePath)
        {
            lock (rootDirectory)
            {
                XDocument xDoc = new XDocument(GetDirXMLElem(rootDirectory));
                xDoc.Save(savePath);
                //MessageBox.Show("Async saving done " + Thread.CurrentThread.ManagedThreadId);
            }
        }

        private XElement GetDirXMLElem(DirectoryElem dir)
        {
            XElement elem = new XElement("dir",
                new XAttribute("name", dir.Name),
                new XAttribute("createTime", dir.CreationTime.ToShortDateString()),
                new XAttribute("length", dir.Length));
            foreach (var subItem in dir.subItems)
            {
                DirectoryElem directory = subItem as DirectoryElem;
                if (directory != null)
                {
                    elem.Add(GetDirXMLElem(directory));
                    continue;
                }
                FileElem file = subItem as FileElem;
                if (file != null)
                {
                    elem.Add(new XElement("file",
                        new XAttribute("name", file.Name),
                        new XAttribute("createTime", file.CreationTime.ToShortDateString()),
                        new XAttribute("length", file.Length)));
                    continue;
                }
            }
            return elem;
        }

        private void DirInfoToTree()
        {
            while (true)
            {
                ScanningDone.WaitOne();
                GenerateTreeDelegate asyncDel = GenerateTree;
                IAsyncResult ar = asyncDel.BeginInvoke(this.treeViewDirContent, null, null);
                asyncDel.EndInvoke(ar);
                ScanningDone.Reset();
                //MessageBox.Show("Tree gen done " + Thread.CurrentThread.ManagedThreadId);
            }
        }

        private void GenerateTree(TreeView tree)
        {
            tree.BeginInvoke(new Action(() =>
            {
                tree.Nodes.Clear();
                tree.Nodes.Add(GetDirNode(rootDirectory));
            }));
            //MessageBox.Show("Async tree gen done " + Thread.CurrentThread.ManagedThreadId);
        }

        private TreeNode GetDirNode(DirectoryElem dir)
        {
            TreeNode dirNode = new TreeNode(dir.Name);
            foreach (var subItem in dir.subItems)
            {
                DirectoryElem directory = subItem as DirectoryElem;
                if (directory != null)
                {
                    dirNode.Nodes.Add(GetDirNode(directory));
                    continue;
                }
                FileElem file = subItem as FileElem;
                if (file != null)
                {
                    dirNode.Nodes.Add(file.Name);
                    continue;
                }
            }
            return dirNode;
        }
    }

    static class ExceptionExtensions
    {
        public static ISynchronizeInvoke PrintException(this ISynchronizeInvoke element, UnauthorizedAccessException ex)
        {
            MessageBox.Show("Ошибка доступа.");
            return element;
        }
    }
}
