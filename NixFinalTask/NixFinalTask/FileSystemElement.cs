﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NixFinalTask
{
    public abstract class FileSystemElem
    {
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public long Length { get; set; }
        public FileSystemElem()
        {

        }
    }

    public class DirectoryElem : FileSystemElem
    {
        public List<FileSystemElem> subItems = new List<FileSystemElem>();
        public DirectoryElem(DirectoryInfo dirInfo, long length = 0)
            : base()
        {
            Name = dirInfo.Name;
            CreationTime = dirInfo.CreationTime;
            Length = length;
        }
        public DirectoryElem()
            : base()
        {

        }
    }
   
    public class FileElem : FileSystemElem
    {
        public FileElem(FileInfo fileInfo)
            : base()
        {
            Name = fileInfo.Name;
            CreationTime = fileInfo.CreationTime;
            Length = fileInfo.Length;
        }
        public FileElem()
            : base()
        {

        }
    }
}
