﻿namespace NixFinalTask
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonChooseDir = new System.Windows.Forms.Button();
            this.buttonSaveToXml = new System.Windows.Forms.Button();
            this.treeViewDirContent = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // buttonChooseDir
            // 
            this.buttonChooseDir.Enabled = false;
            this.buttonChooseDir.Location = new System.Drawing.Point(12, 12);
            this.buttonChooseDir.Name = "buttonChooseDir";
            this.buttonChooseDir.Size = new System.Drawing.Size(127, 23);
            this.buttonChooseDir.TabIndex = 0;
            this.buttonChooseDir.Text = "Choose directory";
            this.buttonChooseDir.UseVisualStyleBackColor = true;
            this.buttonChooseDir.Click += new System.EventHandler(this.buttonChooseDir_Click);
            // 
            // buttonSaveToXml
            // 
            this.buttonSaveToXml.Location = new System.Drawing.Point(145, 12);
            this.buttonSaveToXml.Name = "buttonSaveToXml";
            this.buttonSaveToXml.Size = new System.Drawing.Size(127, 23);
            this.buttonSaveToXml.TabIndex = 1;
            this.buttonSaveToXml.Text = "Select XML";
            this.buttonSaveToXml.UseVisualStyleBackColor = true;
            this.buttonSaveToXml.Click += new System.EventHandler(this.buttonSaveToXml_Click);
            // 
            // treeViewDirContent
            // 
            this.treeViewDirContent.Location = new System.Drawing.Point(12, 41);
            this.treeViewDirContent.Name = "treeViewDirContent";
            this.treeViewDirContent.Size = new System.Drawing.Size(260, 208);
            this.treeViewDirContent.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.treeViewDirContent);
            this.Controls.Add(this.buttonSaveToXml);
            this.Controls.Add(this.buttonChooseDir);
            this.Name = "Form1";
            this.Text = "Final Task";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonChooseDir;
        private System.Windows.Forms.Button buttonSaveToXml;
        private System.Windows.Forms.TreeView treeViewDirContent;
    }
}

