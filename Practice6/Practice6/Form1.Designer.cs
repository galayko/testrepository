﻿namespace Practice6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.mobilePhoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.homePhoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.contactImage = new System.Windows.Forms.PictureBox();
            this.brouseImageButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.isGrouped = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchNameTextBox = new System.Windows.Forms.TextBox();
            this.searchSurnameTextBox = new System.Windows.Forms.TextBox();
            this.filterButton = new System.Windows.Forms.Button();
            this.groupeTextBox = new System.Windows.Forms.TextBox();
            this.contactsTree = new System.Windows.Forms.TreeView();
            this.NameError = new System.Windows.Forms.Label();
            this.SurnameError = new System.Windows.Forms.Label();
            this.MobilePhoneError = new System.Windows.Forms.Label();
            this.HomePhoneError = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.contactImage)).BeginInit();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(12, 344);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(130, 38);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "Добавить";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(152, 344);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(130, 38);
            this.removeButton.TabIndex = 2;
            this.removeButton.Text = "Удалить";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameTextBox.Location = new System.Drawing.Point(288, 12);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(273, 29);
            this.nameTextBox.TabIndex = 3;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.surnameTextBox.Location = new System.Drawing.Point(288, 47);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(273, 29);
            this.surnameTextBox.TabIndex = 4;
            // 
            // mobilePhoneNumberTextBox
            // 
            this.mobilePhoneNumberTextBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mobilePhoneNumberTextBox.Location = new System.Drawing.Point(288, 82);
            this.mobilePhoneNumberTextBox.Name = "mobilePhoneNumberTextBox";
            this.mobilePhoneNumberTextBox.Size = new System.Drawing.Size(273, 29);
            this.mobilePhoneNumberTextBox.TabIndex = 5;
            // 
            // homePhoneNumberTextBox
            // 
            this.homePhoneNumberTextBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.homePhoneNumberTextBox.Location = new System.Drawing.Point(288, 117);
            this.homePhoneNumberTextBox.Name = "homePhoneNumberTextBox";
            this.homePhoneNumberTextBox.Size = new System.Drawing.Size(273, 29);
            this.homePhoneNumberTextBox.TabIndex = 6;
            // 
            // contactImage
            // 
            this.contactImage.Image = ((System.Drawing.Image)(resources.GetObject("contactImage.Image")));
            this.contactImage.Location = new System.Drawing.Point(288, 187);
            this.contactImage.Name = "contactImage";
            this.contactImage.Size = new System.Drawing.Size(448, 151);
            this.contactImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.contactImage.TabIndex = 7;
            this.contactImage.TabStop = false;
            // 
            // brouseImageButton
            // 
            this.brouseImageButton.Location = new System.Drawing.Point(288, 344);
            this.brouseImageButton.Name = "brouseImageButton";
            this.brouseImageButton.Size = new System.Drawing.Size(130, 38);
            this.brouseImageButton.TabIndex = 15;
            this.brouseImageButton.Text = "Выбрать изображение";
            this.brouseImageButton.UseVisualStyleBackColor = true;
            this.brouseImageButton.Click += new System.EventHandler(this.brouseImageButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(431, 344);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(130, 38);
            this.saveButton.TabIndex = 9;
            this.saveButton.Text = "Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // isGrouped
            // 
            this.isGrouped.AutoSize = true;
            this.isGrouped.Location = new System.Drawing.Point(186, 12);
            this.isGrouped.Name = "isGrouped";
            this.isGrouped.Size = new System.Drawing.Size(96, 17);
            this.isGrouped.TabIndex = 10;
            this.isGrouped.Text = "Группировать";
            this.isGrouped.UseVisualStyleBackColor = true;
            this.isGrouped.CheckedChanged += new System.EventHandler(this.isGrouped_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Фильтровать:";
            // 
            // searchNameTextBox
            // 
            this.searchNameTextBox.Location = new System.Drawing.Point(12, 35);
            this.searchNameTextBox.Name = "searchNameTextBox";
            this.searchNameTextBox.Size = new System.Drawing.Size(85, 20);
            this.searchNameTextBox.TabIndex = 12;
            // 
            // searchSurnameTextBox
            // 
            this.searchSurnameTextBox.Location = new System.Drawing.Point(103, 35);
            this.searchSurnameTextBox.Name = "searchSurnameTextBox";
            this.searchSurnameTextBox.Size = new System.Drawing.Size(94, 20);
            this.searchSurnameTextBox.TabIndex = 13;
            // 
            // filterButton
            // 
            this.filterButton.Location = new System.Drawing.Point(203, 32);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(75, 23);
            this.filterButton.TabIndex = 14;
            this.filterButton.Text = "Применить";
            this.filterButton.UseVisualStyleBackColor = true;
            this.filterButton.Click += new System.EventHandler(this.filterButton_Click);
            // 
            // groupeTextBox
            // 
            this.groupeTextBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupeTextBox.Location = new System.Drawing.Point(288, 152);
            this.groupeTextBox.Name = "groupeTextBox";
            this.groupeTextBox.Size = new System.Drawing.Size(273, 29);
            this.groupeTextBox.TabIndex = 8;
            // 
            // contactsTree
            // 
            this.contactsTree.Location = new System.Drawing.Point(12, 61);
            this.contactsTree.Name = "contactsTree";
            this.contactsTree.Size = new System.Drawing.Size(266, 277);
            this.contactsTree.TabIndex = 16;
            this.contactsTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.contactsTree_AfterSelect);
            // 
            // NameError
            // 
            this.NameError.AutoSize = true;
            this.NameError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameError.ForeColor = System.Drawing.Color.Red;
            this.NameError.Location = new System.Drawing.Point(567, 15);
            this.NameError.Name = "NameError";
            this.NameError.Size = new System.Drawing.Size(0, 22);
            this.NameError.TabIndex = 17;
            // 
            // SurnameError
            // 
            this.SurnameError.AutoSize = true;
            this.SurnameError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SurnameError.ForeColor = System.Drawing.Color.Red;
            this.SurnameError.Location = new System.Drawing.Point(567, 50);
            this.SurnameError.Name = "SurnameError";
            this.SurnameError.Size = new System.Drawing.Size(0, 22);
            this.SurnameError.TabIndex = 18;
            // 
            // MobilePhoneError
            // 
            this.MobilePhoneError.AutoSize = true;
            this.MobilePhoneError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MobilePhoneError.ForeColor = System.Drawing.Color.Red;
            this.MobilePhoneError.Location = new System.Drawing.Point(567, 89);
            this.MobilePhoneError.Name = "MobilePhoneError";
            this.MobilePhoneError.Size = new System.Drawing.Size(0, 22);
            this.MobilePhoneError.TabIndex = 19;
            // 
            // HomePhoneError
            // 
            this.HomePhoneError.AutoSize = true;
            this.HomePhoneError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HomePhoneError.ForeColor = System.Drawing.Color.Red;
            this.HomePhoneError.Location = new System.Drawing.Point(567, 120);
            this.HomePhoneError.Name = "HomePhoneError";
            this.HomePhoneError.Size = new System.Drawing.Size(0, 22);
            this.HomePhoneError.TabIndex = 20;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 387);
            this.Controls.Add(this.HomePhoneError);
            this.Controls.Add(this.MobilePhoneError);
            this.Controls.Add(this.SurnameError);
            this.Controls.Add(this.NameError);
            this.Controls.Add(this.contactsTree);
            this.Controls.Add(this.groupeTextBox);
            this.Controls.Add(this.filterButton);
            this.Controls.Add(this.searchSurnameTextBox);
            this.Controls.Add(this.searchNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.isGrouped);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.brouseImageButton);
            this.Controls.Add(this.contactImage);
            this.Controls.Add(this.homePhoneNumberTextBox);
            this.Controls.Add(this.mobilePhoneNumberTextBox);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.contactImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox mobilePhoneNumberTextBox;
        private System.Windows.Forms.TextBox homePhoneNumberTextBox;
        private System.Windows.Forms.PictureBox contactImage;
        private System.Windows.Forms.Button brouseImageButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.CheckBox isGrouped;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchNameTextBox;
        private System.Windows.Forms.TextBox searchSurnameTextBox;
        private System.Windows.Forms.Button filterButton;
        private System.Windows.Forms.TextBox groupeTextBox;
        private System.Windows.Forms.TreeView contactsTree;
        private System.Windows.Forms.Label NameError;
        private System.Windows.Forms.Label SurnameError;
        private System.Windows.Forms.Label MobilePhoneError;
        private System.Windows.Forms.Label HomePhoneError;
    }
}

