﻿using Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml.Linq;

namespace Practice6
{
    public partial class Form1 : Form
    {
        private List<Contact> contactsList;
        private ContactOperations xmlHelper = new ContactOperations();
        private const string NEW_CONTACT_NAME = "[Новый контакт]";
        private Regex noSymbolsRgx = new Regex(@"^[a-zA-Zа-яА-Я]+$");
        private Regex phoneRgx = new Regex(@"^[0-9()\-+ ]+$");
        private string contactsFilePath = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            searchNameTextBox.SetWatermark("Имя");
            searchSurnameTextBox.SetWatermark("Фамилия");
            nameTextBox.SetWatermark("Имя");
            surnameTextBox.SetWatermark("Фамилия");
            mobilePhoneNumberTextBox.SetWatermark("Мобильный тел.");
            homePhoneNumberTextBox.SetWatermark("Домашний тел.");
            groupeTextBox.SetWatermark("Группа");
            ConfigDatabaseFolder();
            contactsList = xmlHelper
                .GetList(contactsFilePath);
            redrawTree(contactsList);
        }

        private void ConfigDatabaseFolder()
        {
            string contactsPath = Directory.GetCurrentDirectory() + @"\Contacts";
            if (!Directory.Exists(contactsPath))
            {
                Directory.CreateDirectory(contactsPath);
            }
            if (!Directory.Exists(contactsPath + @"\Images"))
            {
                Directory.CreateDirectory(contactsPath + @"\Images");
            }
            contactsFilePath = contactsPath + @"\" + ConfigurationManager.AppSettings["contactsPath"];
            if (!File.Exists(contactsFilePath))
            {
                XDocument document = new XDocument();
                document.Add(new XElement("Contacts"));
                document.Save(contactsFilePath);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            contactsTree.Nodes.Add(NEW_CONTACT_NAME);
            contactsTree.SelectedNode = contactsTree.Nodes[contactsTree.Nodes.Count - 1];
        }

        private void isGrouped_CheckedChanged(object sender, EventArgs e)
        {
            redrawTree(contactsList);
        }

        private void redrawTree(List<Contact> treeList)
        {
            if (isGrouped.Checked)
            {
                contactsTree.Nodes.Clear();
                var groups = treeList.Select(x => x.Group).Distinct();
                foreach (string group in groups)
                {
                    if (group == "")
                    {
                        contactsTree.Nodes.Add("Общая группа");
                    }
                    else contactsTree.Nodes.Add(group);
                    foreach (var contact in treeList.Where(x => x.Group == group).ToList())
                    {
                        contactsTree.Nodes[contactsTree.Nodes.Count - 1].Nodes.Add(contact.Surname + ", " + contact.Name);
                    }
                }
            }
            else
            {
                contactsTree.Nodes.Clear();
                foreach (Contact item in treeList)
                {
                    contactsTree.Nodes.Add(
                        item.Name == "" ? NEW_CONTACT_NAME : item.Surname + ", " + item.Name);
                }
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (contactsTree.SelectedNode != null)
            {
                Contact selectedContact;
                if (contactsTree.SelectedNode.Text != NEW_CONTACT_NAME)
                {
                    selectedContact = contactsList
                        .Where(x => x.Surname + ", " + x.Name == contactsTree.SelectedNode.Text)
                        .FirstOrDefault();
                    if (selectedContact == null)
                    {
                        return;
                    }
                    xmlHelper.Remove(selectedContact);
                    contactsList.Remove(selectedContact);
                }
                redrawTree(contactsList);
            }
        }

        private void contactsTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (contactsTree.SelectedNode.Text == NEW_CONTACT_NAME)
            {
                nameTextBox.Text = "";
                surnameTextBox.Text = "";
                homePhoneNumberTextBox.Text = "";
                mobilePhoneNumberTextBox.Text = "";
                groupeTextBox.Text = "";
                contactImage.Load("no-photo.jpg");
                return;
            }

            Contact selectedContact = contactsList.Where(x => x.Surname + ", " + x.Name == contactsTree.SelectedNode.Text).FirstOrDefault();
            if (selectedContact == null)
            {
                return;
            }
            nameTextBox.Text = selectedContact.Name;
            surnameTextBox.Text = selectedContact.Surname;
            homePhoneNumberTextBox.Text = selectedContact.HomePhone;
            mobilePhoneNumberTextBox.Text = selectedContact.MobilePhone;
            groupeTextBox.Text = selectedContact.Group;
            try
            {
                contactImage.Load(selectedContact.Photo);
            }
            catch (Exception)
            {
                contactImage.Load("no-photo.jpg");
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (contactsTree.SelectedNode == null) return;
            if (!isFieldsCorrect()) return;
            if (contactsTree.SelectedNode.Text == NEW_CONTACT_NAME)
            {
                if (nameTextBox.Text == "" || surnameTextBox.Text == "")
                {
                    MessageBox.Show("Поля 'Имя' и 'Фамилия' обязательны для заполнения.");
                    return;
                }
                Contact newCont = new Contact()
                {
                    Name = nameTextBox.Text,
                    Surname = surnameTextBox.Text,
                    HomePhone = homePhoneNumberTextBox.Text,
                    MobilePhone = mobilePhoneNumberTextBox.Text,
                    Group = groupeTextBox.Text
                };
                if(contactImage.ImageLocation != "no-photo.jpg")
                {
                    newCont.Photo = contactImage.ImageLocation;
                }
                contactsList.Add(newCont);
                xmlHelper.Add(newCont);
                contactsTree.SelectedNode.Text = newCont.Name + ", " + newCont.Surname;
                if (isGrouped.Checked)
                {
                    redrawTree(contactsList);
                }
                return;
            }
            Contact selectedContact = contactsList
                .Where(x => x.Surname + ", " + x.Name == contactsTree.SelectedNode.Text)
                .FirstOrDefault();
            if (selectedContact == null)
            {
                return;
            }
            selectedContact.Name = nameTextBox.Text;
            selectedContact.Surname = surnameTextBox.Text;
            selectedContact.HomePhone = homePhoneNumberTextBox.Text;
            selectedContact.MobilePhone = mobilePhoneNumberTextBox.Text;
            selectedContact.Group = groupeTextBox.Text;
            selectedContact.Photo = contactImage.ImageLocation;
            xmlHelper.Edit(contactsList.IndexOf(selectedContact), selectedContact);
        }

        private bool isFieldsCorrect()
        {
            bool isCorrect = true;

            NameError.Text = "";
            SurnameError.Text = "";
            MobilePhoneError.Text = "";
            HomePhoneError.Text = "";

            if (!noSymbolsRgx.IsMatch(nameTextBox.Text))
            {
                NameError.Text = "Имя имеет неверный формат";
                isCorrect = false;
            }
            if (!noSymbolsRgx.IsMatch(surnameTextBox.Text) && SurnameError.Text != "")
            {
                SurnameError.Text = "Фамилия имеет неверный формат";
                isCorrect = false;
            }
            if (!phoneRgx.IsMatch(mobilePhoneNumberTextBox.Text) && mobilePhoneNumberTextBox.Text != "")
            {
                MobilePhoneError.Text = "Телефон имеет неверный формат";
                isCorrect = false;
            }
            if (!phoneRgx.IsMatch(homePhoneNumberTextBox.Text) && homePhoneNumberTextBox.Text != "")
            {
                HomePhoneError.Text = "Домашний телефон имеет неверный формат";
                isCorrect = false;
            }

            return isCorrect;
        }

        private void brouseImageButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialogIcon = new OpenFileDialog();
            if (openDialogIcon.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    contactImage.Load(openDialogIcon.FileName.ToString());
                    File.Copy(openDialogIcon.FileName,
                        Path.Combine(@"Contacts\Images", Path.GetFileName(openDialogIcon.FileName)), true);
                }
                catch (Exception)
                {
                    MessageBox.Show("Изображение выбранно не корректно.");
                }
            }
            saveButton_Click(new object(), new EventArgs());
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            List<Contact> resultList = contactsList
                .Where(x => x.Name.Contains(searchNameTextBox.Text) && x.Surname.Contains(searchSurnameTextBox.Text))
                .ToList();   
            redrawTree(resultList);
        }
    }

    public static class TextBoxWatermarkExtensionMethod
    {
        private const uint ECM_FIRST = 0x1500;
        private const uint EM_SETCUEBANNER = ECM_FIRST + 1;

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        public static void SetWatermark(this TextBox textBox, string watermarkText)
        {
            SendMessage(textBox.Handle, EM_SETCUEBANNER, 0, watermarkText);
        }

    }
}
