﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    public class Contact
    {
        public string Name { set; get; }
        public string Surname { set; get; }
        public string HomePhone { set; get; }
        public string MobilePhone { set; get; }
        public string Group { set; get; }
        public string Photo { set; get; }
    }
}
