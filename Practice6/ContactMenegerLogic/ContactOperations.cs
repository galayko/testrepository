﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace Core
{
    public class ContactOperations
    {
        private string fileName;
        private XDocument xFile;

        public List<Contact> GetList(string FileName)
        {
            fileName = FileName;
            xFile = XDocument.Load(FileName);
            List<Contact> contacts = new List<Contact>();
            if (xFile.Root != null)
            {
                foreach (XElement child in xFile.Root.Elements())
                {
                    contacts.Add(new Contact()
                    {
                        Name = child.Element("Name").Value,
                        Surname = child.Element("Surname").Value,
                        HomePhone = child.Element("HomePhone").Value,
                        MobilePhone = child.Element("MobilePhone").Value,
                        Group = child.Element("Group").Value,
                        Photo = child.Element("Photo").Value
                    });
                }
            }
            return contacts;
        }

        public void Add(Contact newCont)
        {
            XElement srcTree = new XElement("Contact",
                new XElement("Name", newCont.Name),
                new XElement("Surname", newCont.Surname),
                new XElement("HomePhone", newCont.HomePhone),
                new XElement("MobilePhone", newCont.MobilePhone),
                new XElement("Group", newCont.Group),
                new XElement("Photo", newCont.Photo)
            );
            XElement XMLContactsList = xFile.Element("Contacts");
            XMLContactsList.Add(srcTree);
            xFile.Save(fileName, SaveOptions.None);
        }

        public void Remove(Contact item)
        {
            xFile.Root.Elements()
                .Where(x => x.Element("Name").Value == item.Name && x.Element("Surname").Value == item.Surname)
                .First()
                .Remove();
            xFile.Save(fileName);
        }

        public void Edit(int index, Contact item)
        {
            xFile.Root.Elements().ToList()[index].Element("Name").Value = item.Name;
            xFile.Root.Elements().ToList()[index].Element("Surname").Value = item.Surname;
            xFile.Root.Elements().ToList()[index].Element("HomePhone").Value = item.HomePhone;
            xFile.Root.Elements().ToList()[index].Element("MobilePhone").Value = item.MobilePhone;
            xFile.Root.Elements().ToList()[index].Element("Group").Value = item.Group;
            xFile.Root.Elements().ToList()[index].Element("Photo").Value = item.Photo;
            xFile.Save(fileName);
        }
    }
}
