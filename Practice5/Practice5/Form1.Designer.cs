﻿namespace Practice5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exceptionButton = new System.Windows.Forms.Button();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.exceptionButton2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exceptionButton
            // 
            this.exceptionButton.Location = new System.Drawing.Point(12, 12);
            this.exceptionButton.Name = "exceptionButton";
            this.exceptionButton.Size = new System.Drawing.Size(170, 80);
            this.exceptionButton.TabIndex = 0;
            this.exceptionButton.Text = "Создать / Остановить File not found exception";
            this.exceptionButton.UseVisualStyleBackColor = true;
            this.exceptionButton.Click += new System.EventHandler(this.exceptionButton1_Click);
            // 
            // logTextBox
            // 
            this.logTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTextBox.Location = new System.Drawing.Point(188, 12);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.Size = new System.Drawing.Size(368, 252);
            this.logTextBox.TabIndex = 1;
            // 
            // exceptionButton2
            // 
            this.exceptionButton2.Location = new System.Drawing.Point(12, 98);
            this.exceptionButton2.Name = "exceptionButton2";
            this.exceptionButton2.Size = new System.Drawing.Size(170, 84);
            this.exceptionButton2.TabIndex = 2;
            this.exceptionButton2.Text = "Создать еще ошибку / Остановить";
            this.exceptionButton2.UseVisualStyleBackColor = true;
            this.exceptionButton2.Click += new System.EventHandler(this.exceptionButton2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 276);
            this.Controls.Add(this.exceptionButton2);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.exceptionButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exceptionButton;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.Button exceptionButton2;
    }
}

