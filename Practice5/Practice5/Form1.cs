﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Resources;

namespace Practice5
{
    public partial class Form1 : Form
    {
        private static Queue<string> queue;
        private Thread secondThread;
        private Thread thirdThread;
        public bool isRunSecondThread { get; set; }
        public bool isRunThirdThread { get; set; }
        delegate void MsgHandler();
        private static event MsgHandler msgNotifier;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            secondThread = new Thread(() => CheckFileNotFoundEx());
            secondThread.IsBackground = true;
            secondThread.Start();
            thirdThread = new Thread(() => RunThirdThreadException());
            thirdThread.IsBackground = true;
            thirdThread.Start();
            secondThread.Name = "Second thread";
            thirdThread.Name = "Third thread";
            queue = new Queue<string>();
            msgNotifier += new MsgHandler(ShowEx);
        }

        private void RunThirdThreadException()
        {
            while (true)
            {
                if (isRunThirdThread)
                {
                    try
                    {
                        using (StreamReader sr = new StreamReader("TestFile.txt"))
                        {
                            String line = sr.ReadToEnd();
                        }
                    }
                    catch (Exception ex)
                    {
                        lock (queue)
                        {
                            queue.Enqueue(Thread.CurrentThread.Name + ": " + ex.GetType() + "\r\n");
                        }
                        msgNotifier();
                    }
                }
                else
                    Thread.Sleep(100);
            }
        }

        private void CheckFileNotFoundEx()
        {
            while (true)
            {
                if (isRunSecondThread)
                {
                    try
                    {
                        using (StreamReader sr = new StreamReader("TestFile.txt"))
                        {
                            String line = sr.ReadToEnd();
                        }
                    }
                    catch (Exception ex)
                    {
                        lock (queue)
                        {
                            queue.Enqueue(Thread.CurrentThread.Name + ": " + ex.GetType() + "\r\n");
                        }
                        msgNotifier();
                    }
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        private void exceptionButton1_Click(object sender, EventArgs e)
        {
            isRunSecondThread = !isRunSecondThread ? true : false;
        }

        private void exceptionButton2_Click(object sender, EventArgs e)
        {
            isRunThirdThread = !isRunThirdThread ? true : false;
        }

        private void ShowEx()
        {
            lock (queue)
            {
                Action act = () => logTextBox.AppendText(queue.Dequeue());
                if (InvokeRequired)
                {
                    logTextBox.Invoke(act);
                    return;
                }
                logTextBox.AppendText(queue.Dequeue());
            }
        }
    }
}
