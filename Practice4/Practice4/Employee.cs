﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using System.Reflection;

namespace Practice4
{
    public class Employee : IXmlSerializable, IComparable
    {
        private string employeeID;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }

        public string EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public XmlSchema GetSchema()
        {
            return (null);
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            FirstName = reader.ReadElementContentAsString("FirstName", "");
            LastName = reader.ReadElementContentAsString("LastName", "");
            Age = reader.ReadElementContentAsInt("Age", "");
            Department = reader.ReadElementContentAsString("Department", "");
            Address = reader.ReadElementContentAsString("Address", "");
            employeeID = FirstName + LastName;
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("FirstName", FirstName);
            writer.WriteElementString("LastName", LastName);
            writer.WriteElementString("Age", Age.ToString());
            writer.WriteElementString("Department", Department);
            writer.WriteElementString("Address", Address);
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Employee otherEmployee = obj as Employee;
            if (otherEmployee != null)
                return this.employeeID.CompareTo(otherEmployee.employeeID);
            else
                throw new ArgumentException("Object is not a Employee");
        }

        public static List<Employee> DeserializeListFromXmlFile(string filePath, string rootAttribute = "Employees")
        {
            XmlSerializer xml = new XmlSerializer(typeof(List<Employee>), new XmlRootAttribute(rootAttribute));
            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                return (List<Employee>)xml.Deserialize(fs);
            }
        }

        /// <summary>
        /// Я решил не писать тут Console.WriteLine(), т.к. если этот класс потом импортировать в Winforms, то он будет более гибким.
        /// </summary>
        /// <param name="EmployeeList">List Emplyees for print.</param>
        /// <returns></returns>
        public static List<string> GetPrintInfo(List<Employee> EmployeeList)
        {
            List<string> resultList = new List<string>();
            foreach (Employee emp in EmployeeList)
            {
                resultList.Add(string.Format("№{0} Last Name: {1}; First Name: {2}; Department: {3}; Age: {4}; Address: {5}; EmployeeID: {6}",
                    EmployeeList.IndexOf(emp) + 1, emp.LastName, emp.FirstName, emp.Department, emp.Age,
                    typeof(Employee).GetProperty("Address").GetValue(emp, null),
                    typeof(Employee).GetField("employeeID", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(emp)));
            }
            return resultList;
        }

        public static void SerializeListToXmlFile(List<Employee> EmployeesList, string filePath, string rootAttribute = "Employees")
        {
            XmlSerializer xml = new XmlSerializer(typeof(List<Employee>), new XmlRootAttribute(rootAttribute));
            using (var fStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                xml.Serialize(fStream, EmployeesList);
            }
        }
    }
}
