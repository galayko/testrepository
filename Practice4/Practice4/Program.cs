﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using System.Configuration;

namespace Practice4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Задание 4. Брать путь из app.config
            string readFile = ConfigurationManager.AppSettings["readEmployeeFilePath"];
            string writeFile = ConfigurationManager.AppSettings["writeEmployeeFilePath"];

            try
            {
                if (readFile == null)
                {
                    throw new Exception("Не задан файл для считывания.");
                }
                //Десериализация (Задание 1)
                List<Employee> deserializedList = Employee.DeserializeListFromXmlFile(readFile);

                //Задание 2. Вывести на экран информацию о каждом Employee.
                List<string> employeesInStringList = Employee.GetPrintInfo(deserializedList);
                foreach(string element in employeesInStringList)
                {
                    Console.WriteLine(element);
                }

                //Задание 3. Сортировка + Сериализация
                var resultEmpList = deserializedList.Where(x => x.Age > 25 && x.Age < 35).ToList();
                resultEmpList.Sort();
                if (writeFile == null)
                {
                    throw new Exception("Файл для записи не задан.");
                }
                Employee.SerializeListToXmlFile(resultEmpList, writeFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
