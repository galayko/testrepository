﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<LivingBeings> lbList = new List<LivingBeings>(); //d. Создать коллекцию из объектов типа «Живые существа».
            Dog d1 = new Dog("Carl");
            Fish f1 = new Fish("Nemo");
            Horse h1 = new Horse("Fred");
            Animal a1 = new Animal("My animal");
            Crucian c1 = new Crucian("Sara");
            Roach r1 = new Roach("Flint");

            lbList.Add(d1);
            lbList.Add(f1);
            lbList.Add(h1);
            lbList.Add(a1);
            lbList.Add(c1);
            lbList.Add(r1);

            Console.WriteLine("The whole collection:");
            foreach (LivingBeings lb in lbList)
                PrintLivingBeing(lb);

            Console.WriteLine("\nLegs amount: " + GetLegsAmount(lbList));

            PrintBreathUnderWaterId(lbList);
            Console.ReadLine();
        }

        private static void PrintBreathUnderWaterId(List<LivingBeings> lbList) //f.	Реализовать метод который найдет и выведет идентификаторы существ которые могут дышать под водой
        {
            Console.WriteLine("\nIdentifiers of elements, that can breath under water.");
            foreach (LivingBeings lb in lbList)
            {
                if (lb is IBreathUnderWater)
                    PrintLivingBeing(lb);
            }
        }

        private static void PrintLivingBeing(LivingBeings lb)
        {
            Console.WriteLine("Id: {0}, Type: {1}", lb.Identifier, lb.GetType().Name);
        }

        private static string GetLegsAmount(List<LivingBeings> lbList) // e. Реализовать метод который будет считать количество ног у всех элементов этой коллекции.
        {
            int result = 0;
            foreach (LivingBeings lb in lbList)
            {
                Animal animal = lb as Animal;
                if(animal != null)
                    result += animal.Legs;
            }
            return result.ToString();   
        }
    }
}
