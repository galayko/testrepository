﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice3
{
    class Dog : Animal, IBreathUnderWater
    {
        public Dog(string id)
            : base(id)
        {
        }

        public void BreathUnderWater()
        {
            Console.WriteLine("Dog was trying to breath under water.. But..");
        }
    }
}
