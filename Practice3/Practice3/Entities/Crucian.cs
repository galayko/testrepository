﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice3
{
    class Crucian : Fish, IHayEater
    {
        public Crucian(string id)
            : base(id)
        {
        }

        public void EatHay()
        {
            Console.WriteLine("Crucian said that he dont want ur hay!");
        }
    }
}
