﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice3
{
    class Horse : Animal, IHayEater
    {
        public Horse(string id)
            : base(id)
        {
        }

        public void EatHay()
        {
            Console.WriteLine("Hourse it eating hay..");
        }
    }
}
