﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice3
{
    abstract class LivingBeings
    {
        public string Identifier;

        public LivingBeings(string id)
        {
            Identifier = id;
        }
    }
}
