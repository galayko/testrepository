﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice3
{
    class Fish : LivingBeings, IBreathUnderWater
    {
        public Fish(string id)
            : base(id)
        {
        }

        public void BreathUnderWater()
        {
            Console.WriteLine("Fish is breathing...");
        }
    }
}
