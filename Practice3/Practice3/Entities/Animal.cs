﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Practice3
{
    class Animal : LivingBeings
    {
        public int Legs { set; get; }

        public Animal(string id)
            : base(id)
        {
            Legs = 4;
        }
    }
}
