﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture2_practice
{
    class SpeedTest
    {
        private static DateTime start;
        private static TimeSpan resultTime; //Хранит время выполнения части кода
        private static int testingAmount = 1000000;

        enum TestingMode { INT, STRING };

        private static void StartTimer()
        {
            start = DateTime.Now;
        }

        private static void EndTimer()
        {
            DateTime now = DateTime.Now;
            resultTime = now - start;
        }

        public static void IntArrayListSpeedTest()
        {
            Console.WriteLine("\nInt ArrayList speed test started..\n");
            ArrayList intList = new ArrayList();
            TimeSpan funcTime;

            funcTime = AddTesing<ArrayList>(intList, TestingMode.INT);
            PrintTestResult("Adding elements to int ArrayList", funcTime);

            funcTime = GetCodeSpeed(() => intList.Sort());
            PrintTestResult("Sort int list", funcTime);

            funcTime = GetTesing<ArrayList>(intList);
            PrintTestResult("Get elements from int ArrayList", funcTime);
        }

        public static void StringArrayListSpeedTest()
        {
            Console.WriteLine("\nString ArrayList speed test started..\n");
            ArrayList stringList = new ArrayList();
            TimeSpan funcTime;

            funcTime = AddTesing<ArrayList>(stringList, TestingMode.STRING);
            PrintTestResult("Adding elements to string ArrayList", funcTime);

            funcTime = GetCodeSpeed(() => stringList.Sort());
            PrintTestResult("Sort string list", funcTime);

            funcTime = GetTesing<ArrayList>(stringList);
            PrintTestResult("Get elements from string ArrayList", funcTime);
        }

        public static void IntListSpeedTest()
        {
            Console.WriteLine("\nInt List speed test started..\n");
            TimeSpan funcTime;
            List<int> intList = new List<int>();

            funcTime = AddTesing<List<int>>(intList, TestingMode.INT);
            PrintTestResult("Adding elements from int list", funcTime);

            funcTime = GetCodeSpeed(() => intList.Sort());
            PrintTestResult("Sort int list", funcTime);

            funcTime = GetTesing<List<int>>(intList);
            PrintTestResult("Get elements from string list", funcTime);
        }

        public static void StringListSpeedTest()
        {
            Console.WriteLine("\nString List speed test started..\n");
            TimeSpan funcTime;
            List<string> stringList = new List<string>();

            funcTime = AddTesing<List<string>>(stringList, TestingMode.STRING);
            PrintTestResult("Adding elements to string list", funcTime);

            funcTime = GetCodeSpeed(() => stringList.Sort());
            PrintTestResult("Sort string list", funcTime);

            funcTime = GetTesing<List<string>>(stringList);
            PrintTestResult("Get elements from string list", funcTime);
        }

        private static TimeSpan GetTesing<T>(T list) where T : IList
        {
            object temp;
            return GetCodeSpeed(() =>
            {
                for (int i = 0; i < list.Count; i++)
                    temp = list[i];
            });
        }

        private static TimeSpan AddTesing<T>(T list, TestingMode mode) where T : IList, new()
        {
            if (mode == TestingMode.INT)
            {
                return GetCodeSpeed(() =>
                {
                    for (int i = testingAmount; i > 0; i--)
                        list.Add(i);
                });
            }
            else
            {
                return GetCodeSpeed(() =>
                {
                    for (int i = testingAmount; i > 0; i--)
                        list.Add(i + "");
                });
            }
        }

        private static TimeSpan GetCodeSpeed(Action func)
        {
            StartTimer();
            func();
            EndTimer();
            return resultTime;
        }

        private static void PrintTestResult(string testName, TimeSpan time)
        {
            Console.WriteLine("Test: " + testName);
            Console.WriteLine("Time: {0} seconds {1} milliseconds", time.Seconds, time.Milliseconds);
            Console.WriteLine("---------------------------------------------------");
        }
    }
}