﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture2_practice
{
    class GenericClass<T> : IComparable<GenericClass<T>> where T : struct, IComparable<T>
    {
        private T field;

        // Public property specified when creating the type.
        public T Property
        {
            get { return field; }
            set { field = value; }
        }

        public int CompareTo(GenericClass<T> other)
        {
            return this.field.CompareTo(other.field);
        }
    }
}
