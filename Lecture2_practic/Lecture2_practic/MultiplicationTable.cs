﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture2_practice
{
    class MultiplicationTable
    {
        /// <summary>
        /// Checking the diagonal elements for changing the color
        /// </summary>
        /// <returns>If the element is on diagonal</returns>
        static bool IsDiagonal(int i, int j)
        {
            ConsoleColor casualColor = Console.ForegroundColor;
            if (i == j)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(i * j + "\t");
                Console.ForegroundColor = casualColor;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void ForMethod(int upperValue)
        {
            Console.WriteLine("\nТаблица умножения, используя for:\n");

            for (int i = 1; i <= upperValue; i++)
            {
                for (int j = 1; j <= upperValue; j++)
                {
                    if (IsDiagonal(i, j)) continue;
                    Console.Write(i * j + "\t");
                }
                Console.WriteLine();
            }
        }

        public static void DoWhileMethod(int upperValue)
        {
            Console.WriteLine("\nТаблица умножения, используя do..while:\n");

            int i = 1;
            do
            {
                int j = 1;
                do
                {
                    if (IsDiagonal(i, j))
                    {
                        j++;
                        continue;
                    }
                    Console.Write(i * j + "\t");
                    j++;
                }
                while (j <= upperValue);
                Console.WriteLine();
                i++;
            }
            while (i <= upperValue);
        }

        public static void WhileMethod(int upperValue)
        {
            Console.WriteLine("\nТаблица умножения, используя do..while:\n");

            int i = 1;
            while (i <= upperValue)
            {
                int j = 1;
                while (j <= upperValue)
                {
                    if (IsDiagonal(i, j))
                    {
                        j++;
                        continue;
                    }
                    Console.Write(i * j + "\t");
                    j++;
                }
                Console.WriteLine();
                i++;
            }
        }

        public static void ForeachMethod(int upperValue)
        {
            Console.WriteLine("\nТаблица умножения, используя foreach:\n");

            List<int> list = new List<int>();
            for (int i = 1; i < upperValue; i++)
                list.Add(i);

            foreach (int i in list)
            {
                foreach (int j in list)
                {
                    if (IsDiagonal(i, j)) continue;
                    Console.Write(i * j + "\t");
                }
                Console.WriteLine();
            }
        }

    }
}
