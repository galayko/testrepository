﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture2_practice
{

    class GenericClassTest
    {
        public static void Start()
        {
            List<GenericClass<int>> pdList = new List<GenericClass<int>>();
            pdList.Add(new GenericClass<int> { Property = 1 });
            pdList.Add(new GenericClass<int> { Property = 10 });
            pdList.Add(new GenericClass<int> { Property = 4 });

            Console.WriteLine("Elements list before sort:");
            foreach (GenericClass<int> element in pdList)
                Console.WriteLine(element.Property);

            pdList.Sort();

            Console.WriteLine("Elements list after sort:");
            foreach (GenericClass<int> element in pdList)
                Console.WriteLine(element.Property);
        }
    }
}
