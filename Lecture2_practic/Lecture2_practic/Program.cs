﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture2_practice
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Multiplication table testing...\n");
            MultiplicationTable.ForMethod(10);
            MultiplicationTable.DoWhileMethod(10);
            MultiplicationTable.WhileMethod(10);
            MultiplicationTable.ForeachMethod(10);
            Console.WriteLine("\nPractice B testing...\n");
            SpeedTest.IntListSpeedTest();
            SpeedTest.StringListSpeedTest();
            SpeedTest.IntArrayListSpeedTest();
            SpeedTest.StringArrayListSpeedTest();
            Console.WriteLine("\nPractice C testing...\n");
            GenericClassTest.Start();
        }
    }
}
